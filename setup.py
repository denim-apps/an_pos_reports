from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in an_pos_reports/__init__.py
from an_pos_reports import __version__ as version

setup(
	name="an_pos_reports",
	version=version,
	description="Reports and entities for Army Navy\'s POS synchronization.",
	author="JC Gurango",
	author_email="jc@jcgurango.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
