from frappe import _

def get_data():
	return [
		{
			"module_name": "Army Navy POS Reports",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Army Navy POS Reports")
		}
	]
