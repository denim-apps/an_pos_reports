import frappe
import requests
from urllib.parse import urlencode, quote
from datetime import datetime, timedelta

__version__ = '0.0.1'

def get_api_key():
  return frappe.get_value('Army Navy POS Reporting Settings', fieldname='airtable_api_key')

def retrieve_from_airtable(base, table, **kwargs):
  querystring = urlencode(kwargs)

  if querystring:
    querystring = '?' + querystring

  r = requests.get('https://api.airtable.com/v0/' + base + '/' + quote(table) + querystring, headers={
    'Authorization': 'Bearer ' + get_api_key()
  })

  return r.json()

def retrieve_all_from_airtable(base, table, call_per_record=None, call_per_retrieve=None, **kwargs):
  all = []
  offset = -1

  while offset:
    args = kwargs

    if offset != -1:
      args['offset'] = offset

    response = retrieve_from_airtable(
      base,
      table,
      **kwargs
    )

    if response.get('offset'):
      offset = response.get('offset')
    else:
      offset = None

    if call_per_record:
      for record in response['records']:
        call_per_record(record)
        print(record)
    else:
      all.extend(response['records'])

    if call_per_retrieve:
      call_per_retrieve()

  return all

def sync_store(record):
  try:
    store_record = frappe.new_doc('POS Store')
    store_record.code = record['fields'].get('Code')
    store_record.store_name = record['fields'].get('Name')
    store_record.area_manager = record['fields'].get('Area Manager')
    store_record.save(ignore_permissions=True)
  except frappe.exceptions.DuplicateEntryError:
    pass

def sync_sales(record):
  try:
    if record['fields'].get('Sales Type') and record['fields'].get('Store No.'):
      sales_record = frappe.new_doc('Daily Sales Summary')
      sales_record.store = record['fields'].get('Store No.')
      sales_record.date = datetime.strptime(record['fields'].get('Date'), '%d/%m/%Y')
      sales_record.date_key = datetime.strptime(record['fields'].get('Date'), '%d/%m/%Y').strftime('%Y-%m-%d')
      sales_record.transaction_count = int(record['fields'].get('Transaction Count'))
      sales_record.net_amount = float(record['fields'].get('Net Amount').replace('₱', '').replace(',', ''))
      sales_record.gross_amount = float(record['fields'].get('Gross Amount').replace('₱', '').replace(',', ''))
      sales_record.discount_amount = float(record['fields'].get('Discount Amount').replace('₱', '').replace(',', ''))
      sales_record.sales_type = record['fields'].get('Sales Type')
      sales_record.save(ignore_permissions=True)
  except frappe.exceptions.DuplicateEntryError:
    pass

def db_commit():
  frappe.db.commit()

@frappe.whitelist(allow_guest=True)
def sync():
  print('Syncing area managers...')
  area_managers = retrieve_all_from_airtable('appwKA7F1PRuKxMh0', 'Area Managers')
  area_managers = list(map(lambda manager: manager['fields']['Name'], area_managers))

  frappe.db.sql(
    'DELETE FROM `tabArea Manager` WHERE full_name NOT IN ({names})'
      .format(
        names=', '.join(list(map(lambda name: frappe.db.escape(name), area_managers)))
      )
  )

  frappe.db.commit()

  for name in area_managers:
    frappe.db.sql(
      'INSERT INTO `tabArea Manager` (name, full_name) VALUES ({name}, {name}) ON DUPLICATE KEY UPDATE name = name'
        .format(
          name=frappe.db.escape(name)
        )
    )

  frappe.db.commit()

  print('Syncing stores...')
  retrieve_all_from_airtable(
    'appwKA7F1PRuKxMh0',
    'Store Master List',
    call_per_record=sync_store,
    cellFormat='string',
    userLocale='en-gb',
    timeZone='Asia/Singapore'
  )

  frappe.db.commit()

  print('Syncing last day numbers...')
  _24_hours_ago = datetime.now() - timedelta(hours=168)

  retrieve_all_from_airtable(
    'app1xDZBKLoPTl0aO',
    'Daily Summary',
    call_per_record=sync_sales,
    call_per_retrieve=db_commit,
    cellFormat='string',
    userLocale='en-gb',
    timeZone='Asia/Singapore',
    filterByFormula=('1' if frappe.request.args.get('get_all') else '{Modified Date & Time}>"' + _24_hours_ago.isoformat() + '"')
  )

  frappe.db.commit()

  return 'OK'
