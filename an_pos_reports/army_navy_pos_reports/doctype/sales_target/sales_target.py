# Copyright (c) 2021, JC Gurango and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document

class SalesTarget(Document):
	def autoname(self):
		# select a project name based on customer
		self.name = self.brand + ' Sales Target for ' + self.month + ' ' + str(self.year)
