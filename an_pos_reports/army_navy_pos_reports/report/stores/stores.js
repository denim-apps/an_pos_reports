const months = ['January','February','March','April','May','June','July','August','September','October', 'November', 'December'];

frappe.query_reports['Stores'] = {
  'filters': [
    {
      'fieldname': 'month',
      'label': __('Month'),
      'fieldtype': 'Select',
      'options': months,
      'default': months[(new Date()).getMonth()],
    },
    {
      'fieldname': 'year',
      'label': __('Year'),
      'fieldtype': 'Select',
      'options': ['2019','2020','2021','2022','2023','2024','2025','2026','2027','2028','2029','2030'],
      'default': (new Date()).getFullYear(),
    },
  ],
  get_datatable_options: function(options) {
    return {
      ...options,
      serialNoColumn: false,
    };
  },
};
