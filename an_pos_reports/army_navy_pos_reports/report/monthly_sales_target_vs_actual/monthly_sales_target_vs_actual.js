frappe.query_reports['Monthly Sales Target vs Actual'] = {
  'filters': [
    {
      'fieldname': 'year',
      'label': __('Year'),
      'fieldtype': 'Select',
      'options': ['2019','2020','2021','2022','2023','2024','2025','2026','2027','2028','2029','2030'],
      'default': (new Date()).getFullYear(),
    },
  ],
  get_datatable_options: function(options) {
    return {
      ...options,
      serialNoColumn: false,
    };
  },
  onload: function(report) {
    report.page.add_inner_button('View Breakdown by Brand', function() {
      frappe.set_route('query-report', 'Sales Target by Brand');
    });
  },
};
