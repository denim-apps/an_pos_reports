frappe.pages['army-navy-pos-report'].on_page_load = function (wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'New Brands',
		single_column: true
	});
	const refresh_callbacks = [];

	const addReport = function (name, label, isFirst) {
		const report = new frappe.views.QueryReport({
			parent: wrapper,
		});

		report.add_chart_buttons_to_toolbar = function () {

		};

		report.add_card_button_to_toolbar = function () {

		};

		report.set_default_secondary_action = function () {

		};

		report.set_title = function () {

		};

		if (!isFirst) {
			report.show_footer_message = function () {

			};
		}

		report.load = function () {
			this.page.clear_inner_toolbar();
			this.route = frappe.get_route();
			this.page_name = frappe.get_route_str();
			this.report_name = name;
			this.page_title = __(this.report_name);
			this.show_save = false;
			this.menu_items = this.get_menu_items();
			this.datatable = null;
			this.prepared_report_action = "New";

			frappe.run_serially([
				() => this.get_report_doc(),
				() => this.get_report_settings(),
				() => this.refresh_report(),
			]);
		};

		report.setup_report_wrapper = function () {
			if (this.$report) return;

			// Remove border from
			$(".page-head-content").removeClass('border-bottom');

			let page_form = this.page.main.find('.page-form');
			this.$status = $(`<div class="form-message text-muted small"></div>`)
				.hide().insertAfter(page_form);

			if (label) {
				$('<h3>Test</h3>').text(label).appendTo(this.page.main);
			}

			this.$summary = $(`<div class="report-summary"></div>`)
				.hide().appendTo(this.page.main);

			this.$chart = $('<div class="chart-wrapper">').hide().appendTo(this.page.main);

			this.$loading = $(this.message_div('')).hide().appendTo(this.page.main);
			this.$report = $('<div class="report-wrapper">').appendTo(this.page.main);
			this.$message = $(this.message_div('')).hide().appendTo(this.page.main);
			if (this.$report) return;
		};

		refresh_callbacks.push(function () {
			report.setup_progress_bar();
			report.refresh();
		});

		report.show();
	};

	addReport('New Brands Store Count', null, true);
	addReport('New Brands Gross Sales', 'Sales');
	addReport('New Brands Average Sales per Day per Store', 'Average Sales/Day/Store');
	addReport('New Brands Percentage from Sales', '% from Sales');

	this.refresh_button && this.refresh_button.remove();
	this.refresh_button = this.page.add_action_icon("refresh", () => {
		refresh_callbacks.forEach(function (cb) { cb(); });
	});
};
