from . import __version__ as app_version

app_name = "an_pos_reports"
app_title = "Army Navy POS Reports"
app_publisher = "JC Gurango"
app_description = "Reports and entities for Army Navy\'s POS synchronization."
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "jc@jcgurango.com"
app_license = "MIT"

website_redirects = [
  {"source": "/app/target-vs-actual", "target": "/comparison"},
]

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/an_pos_reports/css/an_pos_reports.css"
# app_include_js = "/assets/an_pos_reports/js/an_pos_reports.js"

# include js, css files in header of web template
# web_include_css = "/assets/an_pos_reports/css/an_pos_reports.css"
# web_include_js = "/assets/an_pos_reports/js/an_pos_reports.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "an_pos_reports/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Jinja
# ----------

# add methods and filters to jinja environment
# jinja = {
# 	"methods": "an_pos_reports.utils.jinja_methods",
# 	"filters": "an_pos_reports.utils.jinja_filters"
# }

# Installation
# ------------

# before_install = "an_pos_reports.install.before_install"
# after_install = "an_pos_reports.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "an_pos_reports.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes

# override_doctype_class = {
# 	"ToDo": "custom_app.overrides.CustomToDo"
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"an_pos_reports.tasks.all"
# 	],
# 	"daily": [
# 		"an_pos_reports.tasks.daily"
# 	],
# 	"hourly": [
# 		"an_pos_reports.tasks.hourly"
# 	],
# 	"weekly": [
# 		"an_pos_reports.tasks.weekly"
# 	],
# 	"monthly": [
# 		"an_pos_reports.tasks.monthly"
# 	],
# }

# Testing
# -------

# before_tests = "an_pos_reports.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "an_pos_reports.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "an_pos_reports.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]


# User Data Protection
# --------------------

# user_data_fields = [
# 	{
# 		"doctype": "{doctype_1}",
# 		"filter_by": "{filter_by}",
# 		"redact_fields": ["{field_1}", "{field_2}"],
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_2}",
# 		"filter_by": "{filter_by}",
# 		"partial": 1,
# 	},
# 	{
# 		"doctype": "{doctype_3}",
# 		"strict": False,
# 	},
# 	{
# 		"doctype": "{doctype_4}"
# 	}
# ]

# Authentication and authorization
# --------------------------------

# auth_hooks = [
# 	"an_pos_reports.auth.validate"
# ]

